package com.example.todo.validation;

import org.springframework.validation.Errors;

public class ToDoValidationErrorBuilder {

    public static ToDoValidationError fromBindingErrors(Errors errors) {
        ToDoValidationError error = new ToDoValidationError("Validation failed. "
        + errors.getErrorCount() + " error(s)");
        errors.getAllErrors().forEach(objectError -> error.addValidationError(objectError.getDefaultMessage()));
        return error;
    }
}

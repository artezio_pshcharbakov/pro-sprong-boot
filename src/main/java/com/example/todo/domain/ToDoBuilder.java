package com.example.todo.domain;

public class ToDoBuilder {

    private static ToDoBuilder instance = new ToDoBuilder();
    private String id = null;
    private String description = "";

    public static ToDoBuilder getInstance() {
        return instance;
    }

    public ToDoBuilder withDescription(String desc) {
        this.description = desc;
        return instance;
    }

    public ToDoBuilder withId(String id) {
        this.id = id;
        return instance;
    }

    public ToDo build() {
        ToDo result = new ToDo(this.description);
        if (id != null) {
            result.setId(id);
        }
        return result;
    }
}
